import 'package:flutter_test_freelancer/pages/pages.dart';
import 'package:get/get.dart';
import 'app_routes.dart';

abstract class AppPages {
  static final List<GetPage<dynamic>> pages = [
    GetPage(
      name: Routes.ALERTS,
      page: () => AlertsPage(),
      bindings: [
        BindingsBuilder.put(() => AlertsController()),
      ],
    ),
  ];
}
